# Meu Fiado - Versão 0.3

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Descrição
API desenvolvida em Java Spring com conexão com postgresDB.


## 1 Usuario (Cliente | Vendedor)
Para cadastrar, alterar, remover e atualizar um vendedor, basta realizar toda operação com a flag "vendedor" igual a "true". Dessa forma, a API saberá que se trata de um vendedor e não de um cliente;
Para esclarecimento, "id"s de cliente e vendedores **nunca** se repetirão, ou seja, nunca um cliente poderá ter o mesmo "id" de um vendedor ou de um cliente com um cliente.
#### 1.1 Buscar Usuario
    Requisição para busca de usuarios cadastrados
    
##### 1.1.1 Listar Usuarios
        Metodo: GET
        Rota: /buscar/usuarios
        Response: 
                        [
                            {
                                "id_usuario": 1,
                                "nome": "Jackson",
                                "cpf": "32456789",
                                "senha": "3456789",
                                "email": "23456789@gmail.com",
                                "cep": "34567890",
                                "endereco": "av. sei la",
                                "numero": 1671,
                                "estado": "PB",
                                "cidade": "joao pessoa",
                                "bairro": "manaira",
                                "vendedor": true
                            },
                            {
                                "id_usuario": 2,
                                "nome": "Luciana",
                                "cpf": "32456789",
                                "senha": "3456789",
                                "email": "23456789@gmail.com",
                                "cep": "34567890",
                                "endereco": "av. sei la",
                                "numero": 1671,
                                "estado": "PB",
                                "cidade": "joao pessoa",
                                "bairro": "manaira",
                                "vendedor": false
                            }
                        ]

##### 1.1.2 Id Usuario
        Metodo: GET
        Rota: /buscar/usuario/{id}
        Response:
                        [
                            {
                                "id_usuario": 2,
                                "nome": "Luciana",
                                "cpf": "32456789",
                                "senha": "3456789",
                                "email": "23456789@gmail.com",
                                "cep": "34567890",
                                "endereco": "av. sei la",
                                "numero": 1671,
                                "estado": "PB",
                                "cidade": "joao pessoa",
                                "bairro": "manaira",
                                "vendedor": false
                            }
                        ]


##### 1.1.3 Email Usuario
        Metodo: GET
        Rota: /buscar/usuario?email={email}
        Response:
                        [
                            {
                                "id_usuario": 2,
                                "nome": "Luciana",
                                "cpf": "32456789",
                                "senha": "3456789",
                                "email": "23456789@gmail.com",
                                "cep": "34567890",
                                "endereco": "av. sei la",
                                "numero": 1671,
                                "estado": "PB",
                                "cidade": "joao pessoa",
                                "bairro": "manaira",
                                "vendedor": false
                            }
                        ]
                        
                        
#### 1.2 Criar Usuario
    Metodo: POST
    Rota: /criar/usuario
    Request:
                {
                	"nome": "Jubileu",
                	"cpf": "2145221145",
                	"senha": "12345645",
                	"email": "156465",
                	"cep": "5461231",
                	"endereco": "asfsfrsfr",
                	"numero": 1245,
                	"estado": "cdscss",
                	"cidade": "vdfvfdvfd",
                	"bairro": "csdvfrv",
                    "vendedor": false
                }
                
    Response:   STATUS 200 || STATUS 500
    
#### 1.3 Deletar Usuario
    Metodo: DELETE
    Rota: /deletar/usuario/{id}
    Response:   STATUS 200 || STATUS 500
    
#### 1.4 Atualizar Usuario
    Metodo: PUT
    Rota: /atualizar/usuario/{id}
    Request:
                {
                	"nome": "Jubileu",
                	"cpf": "2145221145",
                	"senha": "12345645",
                	"email": "156465",
                	"cep": "5461231",
                	"endereco": "asfsfrsfr",
                	"numero": 1245,
                	"estado": "cdscss",
                	"cidade": "vdfvfdvfd",
                	"bairro": "csdvfrv",
                    "vendedor": false
                }
                
    Response:   STATUS 200 || STATUS 500
    
    

    
## 2 Lançamentos

#### 2.1 Buscar Lançamento
    Requisição para busca de lançamentos cadastrados
    
##### 2.1.1 Listar Lançamentos
        Metodo: GET
        Rota: /buscar/lancamentos
        Response: 
                [
                    {
                        "idLancamento": 3,
                        "descricao": "Venda do vendedor ;)",
                        "momento_transacao": 1543795200000,
                        "valor": "50000",
                        "idVendedor": {
                            "idUsuario": 7,
                            "nome": "Vendedor",
                            "cpf": "2145221145",
                            "senha": "12345645",
                            "email": "156465",
                            "cep": "5461231",
                            "endereco": "asfsfrsfr",
                            "numero": 1245,
                            "estado": "cdscss",
                            "cidade": "vdfvfdvfd",
                            "bairro": "csdvfrv",
                            "vendedor": true
                        },
                        "idUsuario": {
                            "idUsuario": 8,
                            "nome": "Usuario",
                            "cpf": "2145221145",
                            "senha": "12345645",
                            "email": "156465",
                            "cep": "5461231",
                            "endereco": "asfsfrsfr",
                            "numero": 1245,
                            "estado": "cdscss",
                            "cidade": "vdfvfdvfd",
                            "bairro": "csdvfrv",
                            "vendedor": false
                        }
                    }
                ]

##### 2.1.2 Id Lançamento
        Metodo: GET
        Rota: /buscar/lancamento/{id}
        Response:
                        [
                            {
                                "descricao": "Venda do vendedor ;)",
                                "momento_transacao": "2018-12-03 00:47:22.76221300",
                                "valor": "50000",
                                "id_vendedor": 7,
                                "id_usuario": 8
                            }
                        ]

##### 2.1.2 Id Cliente Lançamento
        Metodo: GET
        Rota: /buscar/lancamento/idcliente/{id}
        Response:
                [
                    {
                        "idLancamento": 1,
                        "descricao": "Venda do Vendedor ;)",
                        "momento_transacao": 1543795200000,
                        "valor": "50000",
                        "idVendedor": {
                            "idUsuario": 7,
                            "nome": "Vendedor",
                            "cpf": "2145221145",
                            "senha": "12345645",
                            "email": "156465",
                            "cep": "54612316",
                            "endereco": "teste",
                            "numero": 1245,
                            "estado": "cdscss",
                            "cidade": "vdfvfdvfd",
                            "bairro": "csdvfrv",
                            "vendedor": true
                        },
                        "idUsuario": {
                            "idUsuario": 8,
                            "nome": "Usuario",
                            "cpf": "2145221145",
                            "senha": "12345645",
                            "email": "156465",
                            "cep": "5461231",
                            "endereco": "asfsfrsfr",
                            "numero": 1245,
                            "estado": "cdscss",
                            "cidade": "vdfvfdvfd",
                            "bairro": "csdvfrv",
                            "vendedor": false
                        }
                    },
                    {
                        "idLancamento": 2,
                        "descricao": "Venda do vendedor ;)",
                        "momento_transacao": 1543798800000,
                        "valor": "50000",
                        "idVendedor": {
                            "idUsuario": 7,
                            "nome": "Vendedor",
                            "cpf": "2145221145",
                            "senha": "12345645",
                            "email": "156465",
                            "cep": "54612316",
                            "endereco": "teste",
                            "numero": 1245,
                            "estado": "cdscss",
                            "cidade": "vdfvfdvfd",
                            "bairro": "csdvfrv",
                            "vendedor": true
                        },
                        "idUsuario": {
                            "idUsuario": 8,
                            "nome": "Usuario",
                            "cpf": "2145221145",
                            "senha": "12345645",
                            "email": "156465",
                            "cep": "5461231",
                            "endereco": "asfsfrsfr",
                            "numero": 1245,
                            "estado": "cdscss",
                            "cidade": "vdfvfdvfd",
                            "bairro": "csdvfrv",
                            "vendedor": false
                        }
                    }
                ]

#### 2.2 Criar Lançamento
    Metodo: POST
    Rota: /criar/lancamento
    Request:
            {
                "descricao": "Venda do vendedor ;)",
                "momento_transacao": "2018-12-03",
                "valor": "50000",
                "idVendedor": {"idUsuario": 7},
                "idUsuario": {"idUsuario": 8}
            }
                
    Response:   STATUS 200 || STATUS 500
                {
                    "idLancamento": 3,
                    "descricao": "Venda do vendedor ;)",
                    "momento_transacao": 1543795200000,
                    "valor": "50000",
                    "idVendedor": {
                        "idUsuario": 7,
                        "nome": null,
                        "cpf": null,
                        "senha": null,
                        "email": null,
                        "cep": null,
                        "endereco": null,
                        "numero": 0,
                        "estado": null,
                        "cidade": null,
                        "bairro": null,
                        "vendedor": false
                    },
                    "idUsuario": {
                        "idUsuario": 8,
                        "nome": null,
                        "cpf": null,
                        "senha": null,
                        "email": null,
                        "cep": null,
                        "endereco": null,
                        "numero": 0,
                        "estado": null,
                        "cidade": null,
                        "bairro": null,
                        "vendedor": false
                    }
                }
    
#### 2.3 Deletar Lancamento
    Metodo: DELETE
    Rota: /deletar/lancamento/{id}
    Response:   STATUS 200 || STATUS 500
    
#### 2.4 Atualizar Lancamento
    Metodo: PUT
    Rota: /atualizar/lancamento/{id}
    Request:
            {
                "descricao": "Venda do Vendedor ;)",
                "momento_transacao": "2018-12-03",
                "valor": "50000",
                "idVendedor": {"idUsuario": 7},
                "idUsuario": {"idUsuario": 8}
            }
                
    Response:   STATUS 200 || STATUS 500
    
    
    
    
    
# ChangeLog
V0.0.1 - 02/12/2018 - Projeto com CRUD lancamento e usuario implementado;
V0.0.2 - 03/12/2018 - Adicionando ao CRUD do usuario a busca dos dados atraves do email;