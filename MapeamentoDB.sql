create table lancamento(
	id_lancamento serial NOT NULL,
	descricao text NOT NULL,
	momento_transacao timestamp NOT NULL,
	valor text NOT NULL,
	id_vendedor bigint NOT NULL,
	id_usuario bigint NOT NULL,
	primary key (id_lancamento),
	foreign key (id_vendedor) references usuario(id_usuario),
	foreign key (id_usuario) references usuario(id_usuario)

);


create table usuario(
	id_usuario serial NOT NULL,
	nome text,
	cpf text,
	senha text,
	email text,
	cep text,
	endereco text,
	numero bigint,
	estado text,
	cidade text,
	bairro text,
	vendedor boolean 
	primary key (id_usuario)
);