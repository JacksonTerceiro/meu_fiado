package springbootstarter.lancamento;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import springbootstarter.usuario.Usuario;

@Entity
@Table(name="lancamento")
public class Lancamento {

	@Id
	@Column(name="id_lancamento")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idLancamento;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="momento_transacao")
	private Date momento_transacao;
	
	@Column(name="valor")
	private String valor;
	
    @ManyToOne
    @JoinColumn(name = "id_vendedor")
	private Usuario idVendedor;
	
    @ManyToOne
    @JoinColumn(name = "id_usuario")
	private Usuario idUsuario;
    
    
    

	public Long getIdLancamento() {
		return idLancamento;
	}

	public void setIdLancamento(Long idLancamento) {
		this.idLancamento = idLancamento;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getMomento_transacao() {
		return momento_transacao;
	}

	public void setMomento_transacao(Date momento_transacao) {
		this.momento_transacao = momento_transacao;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Usuario getIdVendedor() {
		return idVendedor;
	}

	public void setIdVendedor(Usuario idVendedor) {
		this.idVendedor = idVendedor;
	}

	public Usuario getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Usuario idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
	


    
}
