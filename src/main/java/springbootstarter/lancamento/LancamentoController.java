package springbootstarter.lancamento;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import springbootstarter.usuario.Usuario;
import springbootstarter.usuario.UsuarioController;

@RestController
public class LancamentoController {

	
	@Autowired
	private LancamentoRepository lancamentoRepository;

	@GetMapping("/buscar/lancamentos")
	public List<Lancamento> getLancamentos() {
		return (List<Lancamento>) lancamentoRepository.findAll();
	}
	
	@GetMapping("/buscar/lancamento/{id}")
	public Lancamento getLancamentoId(@PathVariable Long id) {
		return lancamentoRepository.findOne(id);
	}
	
	
	
	
	
	
	
	
	
	@GetMapping("/buscar/lancamento/idcliente/{id}")
	public List<Lancamento> getLancamentoIdCliente(@PathVariable Long id) {
		return lancamentoRepository.getLancamentosIdUser(id);
	}
	
    @PostMapping("/criar/lancamento")
    public Lancamento createLancamento(@Valid @RequestBody Lancamento lancamento) {
        Lancamento launch = lancamentoRepository.save(lancamento);
        lancamentoRepository.flush();
        return launch;
    }
    
    @DeleteMapping("/deletar/lancamento/{id}")
    public void deleteLancamento(@PathVariable Long id) {
        lancamentoRepository.delete(id);
        return ;
    }
    
    @PutMapping("/atualizar/lancamento/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Lancamento lancamento, @PathVariable long id) {

    	Lancamento launchRespository = lancamentoRepository.findOne(id);

    	if (launchRespository == null)
    		return ResponseEntity.notFound().build();

    	lancamento.setIdLancamento(id);
    	
    	lancamentoRepository.save(lancamento);
    	lancamentoRepository.flush();

    	return ResponseEntity.noContent().build();
    }
	
	
}
