package springbootstarter.lancamento;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long>{
	
//	public List<Lancamento> findByIdUsuario(Usuario obj_user);
//	
//	public List<Lancamento> findByIdUsuarioId(Long id);
	
	
	@Query(value = "select * from lancamento where id_usuario = ?1", nativeQuery = true)
	public List<Lancamento> getLancamentosIdUser(Long id);

}


