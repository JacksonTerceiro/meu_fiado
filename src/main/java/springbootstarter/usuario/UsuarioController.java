package springbootstarter.usuario;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {
	
	//@RequestMapping(value = "/bar/foo", method = RequestMethod.GET)

	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@GetMapping("/buscar/usuarios")
	public List<Usuario> getUsuarios() {
		return (List<Usuario>) usuarioRepository.findAll();
	}
	
	@GetMapping("/buscar/usuario/{id}")
	public Usuario getUsuarioId(@PathVariable Long id) {
		return usuarioRepository.findOne(id);
	}
	
//	@GetMapping(value = "/buscar/usuario/email/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
//	public Usuario getUsuarioEmail(@PathVariable String email) {
//		System.out.println(email);
//		return usuarioRepository.findByEmail(email);
//	}
	
	@GetMapping(value = "/buscar/usuario", produces = MediaType.APPLICATION_JSON_VALUE)
	public Usuario getUsuarioEmail(@RequestParam String email) {
		System.out.println(email);
		return usuarioRepository.findByEmail(email);
	}
	
	
	
    @PostMapping("/criar/usuario")
    public Usuario createUsuario(@Valid @RequestBody Usuario usuario) {
        Usuario user = usuarioRepository.save(usuario);
        usuarioRepository.flush();
        return user;
    }
    
    @DeleteMapping("/deletar/usuario/{id}")
    public void deleteUsuario(@PathVariable Long id) {
        usuarioRepository.delete(id);
        return ;
        //return usuarioRepository.flush();
    }
    
    @PutMapping("/atualizar/usuario/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Usuario usuario, @PathVariable long id) {

    	Usuario userRespository = usuarioRepository.findOne(id);

    	if (userRespository == null)
    		return ResponseEntity.notFound().build();

    	usuario.setIdUsuario(id);
    	
    	usuarioRepository.save(usuario);

    	return ResponseEntity.noContent().build();
    }
}
