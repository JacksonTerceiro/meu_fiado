package springbootstarter.usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
//	@Query(value = "select u from usuario u where u.email = ?1", nativeQuery = true)
//	public Usuario getUserEmail1(String emailUser);
	
	
	public Usuario findByEmail(String email);
	
	
}
