package springbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeuFiadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeuFiadoApplication.class, args);

	}

}
